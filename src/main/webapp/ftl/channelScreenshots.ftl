<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="Cache-Control" content="no-cache, must-revalidate">
    <meta http-equiv="expires" content="0">
    <title>JobTracker Center</title>
    <link rel="stylesheet" type="text/css" href="${rc.contextPath}/css/common.css">
    <script src="${rc.contextPath}/js/jquery.min.js" type="text/javascript"></script>
    <script src="${rc.contextPath}/js/channelScreenshots.js" type="text/javascript"></script>
</head>
<body>
<!--header-->
<div id="header">
    <div id="logo">
        JobTracker Center
    </div>
</div>
<!--header end-->
<div style="margin-top:60px;margin-left:1%;margin-right: 1%;font-size:14px;font-family:'arial';">
    <p align="center" style="font-size:20px;font-family:'arial';">Normal Channel Capture  Info : ${channelModelList?size} at server last 1 day in city:
        <select id="selectChannelCity" onchange=updateChannelCity(this.value) style="width:150px;height:20px">
        <#if firstCity == 0>
        <option  value="All"  selected="selected">All</option>
        <#else>
            <option  value="All">All</option>
        </#if>
        <#if cityList??>
            <#list cityList as cm>
                <#if firstCity == cm.city>
                    <option  value="${cm.city}"  selected="selected">${cm.city}</option>
                <#else>
                    <option  value="${cm.city}" >${cm.city}</option>
                </#if>
            </#list>
        <#else>
        </#if>

        </select>
    </p>
    <br />
    <div id="channelScreenshots" style="width:100%;padding-left:10px;box-sizing:border-box;padding-right:10px;">
        <table width="100%" border=1 class="list" id="channelScreenshotsList">
            <#list channelModelList as channel>
                <#if channel_index%4!=0>
                    <td title="${channel.number}">
                        <p align="left">
                            <#if (channel.backup)>
                                <a href="${channel.captureUrl}/${channel.uuid}/screenshot?isBackup=true" target="_blank"><img src="${channel.captureUrl}/${channel.uuid}/screenshot?isBackup=true"  width="320" height="240" alt="Channel_Number:${channel.number}" /></a>
                            <#else>
                                <a href="${channel.captureUrl}/${channel.uuid}/screenshot?isBackup=false" target="_blank"><img src="${channel.captureUrl}/${channel.uuid}/screenshot?isBackup=false"  width="320" height="240" alt="Channel_Number:${channel.number}" /></a>
                            </#if>
                        </p>
                        <p align="left" style="font-size: 14px">Channel Name:${channel.name}</p>
                        <p align="left" style="font-size: 14px">Channel DisplayName:${channel.displayName}</p>
                        <p align="left" style="font-size: 14px">City:${channel.city},&nbsp;&nbsp;Backup:${channel.backup?string}</p>
                        <p align="left" style="font-size: 14px">Uuid:${channel.uuid}</p>
                    </td>
                <#else>
                    <#if channel_index!=0></tr></#if>
                    <tr style="height: 40px;text-align: center" >
                    <td title="${channel.number}">
                        <p align="left">
                            <#if (channel.backup)>
                                <a href="${channel.captureUrl}/${channel.uuid}/screenshot?isBackup=true" target="_blank"><img src="${channel.captureUrl}/${channel.uuid}/screenshot?isBackup=true"  width="320" height="240" alt="Channel_Number:${channel.number}" /></a>
                            <#else>
                                <a href="${channel.captureUrl}/${channel.uuid}/screenshot?isBackup=false" target="_blank"><img src="${channel.captureUrl}/${channel.uuid}/screenshot?isBackup=false"  width="320" height="240" alt="Channel_Number:${channel.number}" /></a>
                            </#if>

                        </p>
                        <p align="left" style="font-size: 14px">Channel Name:${channel.name}</p>
                        <p align="left" style="font-size: 14px">Channel DisplayName:${channel.displayName}</p>
                        <p align="left" style="font-size: 14px">City:${channel.city},&nbsp;&nbsp;Backup:${channel.backup?string}</p>
                        <p align="left" style="font-size: 14px">Uuid:${channel.uuid}</p>
                    </td>

                </#if>
            </#list>
        </table>
    </div>

    <div id="contextPath" style="display:none;">${rc.contextPath}</div>
        <!--footer-->
    <#include "_footer.ftl" />
        <!--footer end-->
</body>
</html>