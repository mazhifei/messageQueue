<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="Cache-Control" content="no-cache, must-revalidate">
    <meta http-equiv="expires" content="0">

    <link rel="stylesheet" type="text/css" href="${rc.contextPath}/css/common.css">
    <script src="${rc.contextPath}/js/jquery.min.js" type="text/javascript"></script>
    <title>User Login</title>
</head>

<body style="background:rgb(207,224,245)">

<div id="header">
    <div id="logo">Message Queue Center</div>
</div>

<div id="contentBody">
    <table id="loginTable" >
        <tr>
            <td>
                <div id="content">
                    <div id="loginTitle">
                        <img src="${rc.contextPath}/images/loginicon.png" id="imgTitle"/>User Login
                    </div>

                    <form action="${rc.contextPath}/user/login" method="post">
                        <div id="formDiv">
                            <div class="form_user">
                                <div class="form_user_txt">
                                    UserName：
                                </div>
                                <input type="text" class="input_user_txt" name="j_username">
                            </div>

                            <div class="form_user">
                                <div class="form_user_txt">
                                    Password：
                                </div>
                                <input type="password" class="input_user_txt" name="j_password">
                            </div>

                            <input type="submit" value="Login" id="inputSubmit">
                        </div>
                    </form>
                </div>

            </td>
        </tr>
    </table>
</div>
</body>

</html>