<html>
<head>
  <script src="${rc.contextPath}/js/jquery.min.js" type="text/javascript"></script>
    <script src="${rc.contextPath}/js/api.tool.js" type="text/javascript"></script>
    <title>JobTracker Center API Tools</title>
    <link rel="stylesheet" type="text/css" href="${rc.contextPath}/css/common.css">
</head>
<body>
<!--header-->
<#include "_header.ftl" />
<!--header end-->
<div style="margin-top:80px;margin-left:10%;margin-right: 10%;font-size:14px;font-family:'arial';">
    <h2 align="center" >RabbitMQ API</h2>
    <table width="100%">
        <tr style="background-color: #c8c8c8">
            <th width="30%">Method</th>
            <th width="30%">Parameter</th>
            <th width="40%">Result</th>
        </tr>
        <tr>
            <td><input id="tool_queue_resend_put" onclick="tool_queue_resend_put()" type="button" value="/tool/queue/resend/{from-to}_put" class="buttonCss"></td>
            <td>
                <select id="resList" style="width:220px;height:20px;margin:5px;">
                <option value=-1>-resList-</option>
                </select>(required)
                <br/>
                <select id="resDeadList" style="width:220px;height:20px;margin:5px;">
                <option value=-1>-resDeadList-</option>
                </select>(required)
                <br/>
                <input type="text" style="width:220px;height:20px;margin:5px;" id="queueLimit" placeholder="queueLimit">
            </td>
            <td><div id="tool_queue_resend_put_txt" class="resultTxt"></div></td>
        </tr>
        <tr>
            <td><input id="tool_queue_revive_put" onclick="tool_queue_revive_put()" type="button" value="/tool/queue/revive/{queue}_put" class="buttonCss"></td>
            <td>
                <select id="reviveList" style="width:220px;height:20px;margin:5px;">
                    <option value=-1>-resDeadList-</option>
                </select>(required)
                <br/>
                <input type="text" style="width:220px;height:20px;margin:5px;" id="reviveLimit" placeholder="reviveLimit">
            </td>
            <td><div id="tool_queue_revive_put_txt" class="resultTxt"></div></td>
        </tr>
        <tr>
    </table>
    <div id="contextPath" style="display:none;">${rc.contextPath}</div>
        <!--footer <#include "_footer.ftl" />-->

        <!--footer end-->
</body>
</html>
