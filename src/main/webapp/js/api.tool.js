$(function(){
    tool_queue_list_post();
    user_islogin();
})
function user_islogin() {
    var user = $("#user").text();
    var contextPath = $("#contextPath").text();
    if(user == null || user == undefined || user == '')
    {
        alert("Please Login");
        window.location.href=contextPath;
    }
}

function manual_record_post() {
    if(confirm("Are you sure?")){
        var contextPath = $("#contextPath").text();
        $.ajax({
            url:contextPath+"/manual/record",
            type:"post",
            success:function(data) {
                document.getElementById("manual_record_post_txt").innerHTML=data;
            }
        });
    }
}

function manual_record_post_test() {
        var contextPath = $("#contextPath").text();
        $.ajax({
            url:contextPath+"/manual/record_test",
            type:"post",
            success:function(data) {
                document.getElementById("manual_record_post_test_txt").innerHTML=data;
            }
        });
}

function manual_record_get() {
    var contextPath = $("#contextPath").text();
    var record_id = $("#manual_record_get_id").val();
    if(record_id==null||record_id==""){
        alert("Please input record_id");
        return;
    }
    $.ajax({
        url:contextPath+"/manual/record/"+record_id,
        type:"get",
        success:function(data) {
            document.getElementById("manual_record_get_txt").innerHTML=data;
        }
    });
}

function manual_record_uuid() {
    var contextPath = $("#contextPath").text();
    var record_uuid = $("#record_uuid").val();
    var record_url,record_url1_html,record_url2_html;
    if(record_uuid==null||record_uuid==""){
        alert("Please input record_uuid");
        return;
    }
    var str1=record_uuid.substring(9, 11);
    var str2=record_uuid.substring(12, 14);
    record_url = "video url:"+str1+"/"+str2+"/"+record_uuid+"/";
    record_url1_html = "<a href='http://54.184.255.94/videoManager/video/"+record_uuid+"' target='_blank'>http://54.184.255.94/videoManager/video/"+record_uuid+"</a>"
    record_url2_html = "<a href='http://192.168.5.144:8090/videoManager/video/"+record_uuid+"' target='_blank'>http://192.168.5.144:8090/videoManager/video/"+record_uuid+"</a>"
    document.getElementById("manual_record_uuid_txt").innerHTML=record_url+"</br>"+record_url1_html+"</br>"+record_url2_html;
}

function manual_record_reset() {
    var contextPath = $("#contextPath").text();
    var record_id = $("#manual_record_reset_id").val();
    if(record_id==null||record_id==""){
        alert("Please input record_id");
        return;
    }
    $.ajax({
        url:contextPath+"/manual/record_reset/"+record_id,
        type:"get",
        success:function(data) {
            document.getElementById("manual_record_reset_txt").innerHTML=data;
        }
    });
}

function manual_record_put() {
    if(confirm("Are you sure?")){
        var contextPath = $("#contextPath").text();
        $.ajax({
            url:contextPath+"/manual/record",
            type:"put",
            success:function(data) {
                document.getElementById("manual_record_put_txt").innerHTML=data;
            }
        });
    }
}

function manual_record_put_test() {
        var contextPath = $("#contextPath").text();
        $.ajax({
            url:contextPath+"/manual/record_test",
            type:"put",
            success:function(data) {
                document.getElementById("manual_record_put_test_txt").innerHTML=data;
            }
        });
}

function manual_record_delete() {
    if(confirm("Are you sure?")) {
        var contextPath = $("#contextPath").text();
        $.ajax({
            url:contextPath+"/manual/record",
            type:"delete",
            success:function(data) {
                document.getElementById("manual_record_delete_txt").innerHTML=data;
            }
        });
    }
}

function manual_record_delete_test() {
        var contextPath = $("#contextPath").text();
        $.ajax({
            url:contextPath+"/manual/record_test",
            type:"delete",
            success:function(data) {
                document.getElementById("manual_record_delete_test_txt").innerHTML=data;
            }
        });
}

function manual_transfer_post() {
    if(confirm("Are you sure?")) {
        var contextPath = $("#contextPath").text();
        $.ajax({
            url:contextPath+"/manual/transfer",
            type:"post",
            success:function(data) {
                document.getElementById("manual_transfer_post_txt").innerHTML=data;
            }
        });
    }
}

function manual_transfer_post_test() {
        var contextPath = $("#contextPath").text();
        $.ajax({
            url:contextPath+"/manual/transfer_test",
            type:"post",
            success:function(data) {
                document.getElementById("manual_transfer_post_test_txt").innerHTML=data;
            }
        });
}

function tool_queue_list_post(){
    var contextPath = $("#contextPath").text();
    $.ajax({
        url:contextPath+"/tool/queue/list",
        type:"get",
        dataType:"json",
        success:function(data) {
            var resList = data[0];
            var allList = data[1];
            var resDeadList = data[2];
            for(var i = 0;i < allList.length;i++) {
                $("#resList").append("<option value=" + i + ">" + allList[i].name +",Size:"+ allList[i].messages_ready + "</option>");
            }
            for(var i = 0;i < allList.length;i++) {
                $("#resDeadList").append("<option value=" + i + ">" + allList[i].name +",Size:"+ allList[i].messages_ready + "</option>");
            }
            for(var i = 0;i < resDeadList.length;i++) {
                $("#reviveList").append("<option value=" + i + ">" + resDeadList[i].name +",Size:"+ resDeadList[i].messages_ready + "</option>");
            }
        }
    });
}
function tool_queue_resend_put() {
    var contextPath = $("#contextPath").text();
    var _resListValue = $("#resList option:selected").text();
    var _resDeadListValue = $("#resDeadList option:selected").text();
    if(_resListValue == "-resList-") {
        alert("Please choose resList");
        return;
    }
    if(_resDeadListValue == "-resDeadList-") {
        alert("Please choose resDeadList");
        return;
    }
    var resListValue =_resListValue.substring(0,_resListValue.indexOf(','));
    var resDeadListValue = _resDeadListValue.substring(0,_resDeadListValue.indexOf(','));
    var queueLimitValue = $("#queueLimit").val();
    //var data = "resList=" + resList + "&resDeadList=" + resDeadList ;
    var data = "limit="+queueLimitValue;

    $.ajax({
        url:contextPath+"/tool/queue/resend/"+resListValue+"/"+resDeadListValue,
        type:"put",
        data:data,
        success:function(data) {
            document.getElementById("tool_queue_resend_put_txt").innerHTML=data;
        }
    });
}
function tool_queue_revive_put() {
    var contextPath = $("#contextPath").text();
    var _reviveDeadListValue = $("#reviveList option:selected").text();
    if(_reviveDeadListValue == "-resDeadList-") {
        alert("Please choose reviveList");
        return;
    }
    var reviveDeadListValue = _reviveDeadListValue.substring(0,_reviveDeadListValue.indexOf(','));
    var reviveListValue = reviveDeadListValue.substr(0, reviveDeadListValue.length-5);
    var reviveLimitValue = $("#reviveLimit").val();
    var data = "limit="+reviveLimitValue;
    $.ajax({
        url:contextPath+"/tool/queue/revive/"+reviveListValue,
        type:"put",
        data:data,
        success:function(data) {
            document.getElementById("tool_queue_revive_put_txt").innerHTML=data;
        }
    });
}