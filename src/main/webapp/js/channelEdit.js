var tempArray=[];
$(function(){
    user_islogin();
    setVariableShowTotal();

})
function recordingAll(){
    if($("#candidateChannelTableList input:checkbox[name=recordingAll]").is(':checked')){
        $("#candidateChannelTableList input:checkbox[name=recording]").each(function(){
            $(this).attr("checked",true);
        });
    }else{
        $("#candidateChannelTableList input:checkbox[name=recording]").each(function(){
            $(this).attr("checked",false);
        });
    }

}
function selectedAll(){
    if($("#selectChannelTableList input:checkbox[name=selectedAll]").is(':checked')){
        $("#selectChannelTableList input:checkbox[name=selected]").each(function(){
            $(this).attr("checked",true);
        });
    }else{
        $("#selectChannelTableList input:checkbox[name=selected]").each(function(){
            $(this).attr("checked",false);
        });
    }
}

function setVariableShowTotal(){
    var variableTotal=getVariableTotal();
    var showTotal=getShowTotal();
    var backTotal=getBackupTotal();
    $("#VariableShowTotal").html("Variable Total:"+variableTotal+",Show Total:"+showTotal+",Backup Total:"+backTotal);
}

function getVariableTotal(){
    var i=0;
    $("#selectChannelTableList input:checkbox[name=variable]:checked").each(function(){
        i++;
    });
    return i;
}

function getShowTotal(){
    var i=0;
    $("#selectChannelTableList input:checkbox[name=show]:checked").each(function(){
        i++;
    });
    return i;
}

function getBackupTotal(){
    var i=0;
    $("#selectChannelTableList input:checkbox[name=backup]:checked").each(function(){
        i++;
    });
    return i;
}
function user_islogin() {
    var user = $("#user").text();
    var contextPath = $("#contextPath").text();
    if(user == null || user == undefined || user == '')
    {
        alert("Please Login");
        window.location.href=contextPath;
    }
}
function addTempArray(){
    var arrayTemp = $("#selectChannelTableList input:checkbox[name=selected]").map(function () {
        return {
            "cell7": $.trim($(this).closest("tr").find("td:eq(7)").text())
        };
    }).get();
    $.each(arrayTemp, function (i2, d2) {
        var channelId=d2.cell7;
        var bool=true;
        for(var i=0;i<tempArray.length;i++)
        {
            if(channelId==tempArray[i])
            {
                //alert("Please Choose again!"+tempArray[i])
                bool=false;
                break;
            }
        }
        if(bool){
            tempArray.push(channelId);
        }
    })
}


function addChannel(){

    addTempArray();

    var arrayAdd = $("#candidateChannelTableList input:checkbox[name=recording]:checked").map(function () {
        return {
            "cell1": $.trim($(this).closest("tr").find("td:eq(1)").text()),
            "cell2": $.trim($(this).closest("tr").find("td:eq(2)").text()),
            "cell3": $.trim($(this).closest("tr").find("td:eq(3)").html()),
            "cell4": $.trim($(this).closest("tr").find("td:eq(4)").text()),
            "cell5": $.trim($(this).closest("tr").find("td:eq(5)").text()),
            "cell6": $.trim($(this).closest("tr").attr("title")),
            "cell7": $.trim($(this).closest("tr").attr("class"))
        };
    }).get();

    if(arrayAdd == null || arrayAdd == undefined || arrayAdd == '')
    {
        alert("Please Choose Candidate Channels!");
    }

    $.each(arrayAdd, function (i1, d1) {
        var channelId=d1.cell5;
        var bool=true;
        for(var i=0;i<tempArray.length;i++)
        {
            if(channelId==tempArray[i])
            {
                alert("Channel Number "+d1.cell1+",Channel Name "+d1.cell2+" has been chosen!")
                bool=false;
                break;
            }
        }
        if(bool){
            $("#selectChannelTableList").append("<tr align='center' title='"+d1.cell6+"' class='"+d1.cell7+"'><td><input type='checkbox' checked='checked' name='selected'></td> <td>" + d1.cell1 + "</td> <td>" + d1.cell2 + "</td> <td>" + d1.cell3 + "</td> <td>" + d1.cell4 + "</td><td><input type='checkbox' name='variable'></td> <td><input type='checkbox' checked='checked' name='show'</td> <td><input type='checkbox' name='backup'</td> <td style='display:none;'>"+ d1.cell5 + "</td></tr>");
        }
    })

    addTempArray();

    setVariableShowTotal();
    $("#SelectedTotal").html(tempArray.length);
}


function removeChannel(){
    var contextPath = $("#contextPath").text();

    var arrayRemove = $("#selectChannelTableList input:checkbox[name=selected]").map(function () {
        return {
            "cell7": $.trim($(this).closest("tr").find("td:eq(7)").text())
        };
    }).get();
    $.each(arrayRemove, function (i2, d2) {
        var channelId=d2.cell7;
        tempArray.splice($.inArray(channelId,tempArray),1);
    })

    var bool=true;
    $("#selectChannelTableList input:checkbox[name=selected]:checked").each(
        function()
        {
            var id= $(this).val();
            $(this).closest('tr').remove();
            bool=false;
        });
    if(bool){
        alert("Please Choose Selected Channels!");
    }

    addTempArray();

    setVariableShowTotal();
    $("#SelectedTotal").html(tempArray.length);
}
function saveChannel(){
    var companyId=$("#selectCompany").val();
    var contextPath = $("#contextPath").text();
    var arraySave = $("#selectChannelTableList input:checkbox[name=selected]").map(function () {
        return {
            "cell5": $.trim($(this).closest("tr").find("input:checkbox[name=variable]:checked").val()),
            "cell6": $.trim($(this).closest("tr").find("input:checkbox[name=show]:checked").val()),
            "cell7": $.trim($(this).closest("tr").find("input:checkbox[name=backup]:checked").val()),
            "cell8": $.trim($(this).closest("tr").find("td:eq(8)").text())
        };
    }).get();
    var channelList = [];
    var channel = {};
    if(arraySave == null || arraySave == undefined || arraySave == '')
    {
        channel.channelId=-1;
        channel.companyId=companyId;
        $.ajax({
            url:contextPath+"/user/savechannel",
            type:"POST",
            data:channel,
            success:function(data) {
                alert(data);
                window.location.reload();
            }
        });
    }else{
        $.each(arraySave, function (i2, d2) {
            var channel = {};
            var isVariable=false;
            if(d2.cell5=="on"){
                isVariable=true;
            }else{
                isVariable=false;
            }
            var isShow=false;
            if(d2.cell6=="on"){
                isShow=true;
            }else{
                isShow=false;
            }
            var isBackup=false;
            if(d2.cell7=="on"){
                isBackup=true;
            }else{
                isBackup=false;
            }
            var channelId=d2.cell8;
            channel.channelId=channelId;
            channel.companyId=companyId;
            channel.isVariable = isVariable;
            channel.isShow = isShow;
            channel.isBackup = isBackup;
            channelList.push(channel);
        })

        $.ajax({
            url:contextPath+"/user/savechannellist",
            type:"POST",
            data:{"savechannellist":JSON.stringify(channelList)},
            success:function(data) {
                alert(data);
                window.location.reload();
            }
        });
    }

}
function updateCompany(companyid){
    var contextPath = $("#contextPath").text();
    //$.ajax({
    //    url:contextPath+"/user/company",
    //    type:"POST",
    //    data:"companyid="+companyid,
    //    success:function(data) {
    //        window.location.reload();
    //    }
    //});
    window.location.href=contextPath+"/user/company/"+companyid;
}
