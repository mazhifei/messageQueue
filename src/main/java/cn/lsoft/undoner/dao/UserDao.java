package cn.lsoft.undoner.dao;

import cn.lsoft.undoner.model.User;

import java.util.List;

public interface UserDao {
    public List<User> queryUserByUser(User user) ;
}
