package cn.lsoft.undoner.dao;

import cn.lsoft.undoner.model.MessageRequest;

import java.util.List;

/**
 * @author <A HREF="mailto:undoner@gmail.com">undoner</A>
 * @date 05-06-2014
 */


public interface MessageDao {
    public void saveMessage(MessageRequest request);
    public List<MessageRequest> queryMessageList();
}
