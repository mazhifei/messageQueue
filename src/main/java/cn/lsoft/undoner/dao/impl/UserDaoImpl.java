package cn.lsoft.undoner.dao.impl;

import cn.lsoft.undoner.dao.UserDao;
import cn.lsoft.undoner.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.ibatis.SqlMapClientTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by <A HREF="mailto:undoner@gmail.com">undoner</A> on 15-12-8.
 */
@Repository
public class UserDaoImpl implements UserDao{
    @Autowired
    private SqlMapClientTemplate sqlMapClientTemplate;

    @Override
    public List<User> queryUserByUser(User user) {
        return sqlMapClientTemplate.queryForList("UserDaoImpl.queryUserByUser",user);
    }
}
