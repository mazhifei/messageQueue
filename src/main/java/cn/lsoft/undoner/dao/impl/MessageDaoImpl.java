package cn.lsoft.undoner.dao.impl;

import cn.lsoft.undoner.dao.MessageDao;
import cn.lsoft.undoner.model.MessageRequest;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.ibatis.SqlMapClientTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class MessageDaoImpl implements MessageDao {

    private static final Logger LOGGER = Logger.getLogger(MessageDaoImpl.class);

    @Autowired
    private SqlMapClientTemplate sqlMapClientTemplate;

    @Override
    public void saveMessage(MessageRequest message) {
         sqlMapClientTemplate.insert("MessageDaoImpl.saveMessage", message);
        }

    @Override
    public List<MessageRequest> queryMessageList() {
        return sqlMapClientTemplate.queryForList("MessageDaoImpl.queryMessageList");
    }
}
