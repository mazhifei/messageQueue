package cn.lsoft.undoner.model;
/**
 * @author <A HREF="mailto:undoner@gmail.com">undoner</A>
 * @date 19-05-2015
 */
public class User {
	private int id;
	private String userName;
	private String passWord;
	private int userType;
	private String description;
	private String createdAt;
	private String updatedAt;
	
	public User() {}
	
	public User(String userName, String password) {
		this.userName = userName;
		this.passWord = password;
	}

	public User(int id, String userName, String password, int userType, String description, String createdAt, String updatedAt) {
		this.id = id;
		this.userName = userName;
		this.passWord = password;
		this.userType = userType;
		this.description = description;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassWord() {
		return passWord;
	}

	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}

	public int getUserType() {
		return userType;
	}

	public void setUserType(int userType) {
		this.userType = userType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}
}
