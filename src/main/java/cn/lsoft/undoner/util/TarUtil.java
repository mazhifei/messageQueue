package cn.lsoft.undoner.util;

/**
 * <A HREF="mailto:undoner@gmail.com">undoner</A>
 */
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.GZIPInputStream;
import org.apache.tools.tar.TarEntry;
import org.apache.tools.tar.TarInputStream;

public class TarUtil {
    public TarUtil() {
    }

    public static void unTar(String inputPath) {
        unTar((String)inputPath, (String)null);
    }

    public static void unTar(String inputPath, String destDir) {
        boolean result = false;

        try {
            File e = new File(inputPath);
            if(null == destDir) {
                destDir = e.getParentFile().getAbsolutePath();
            }

            unTar(e, destDir);
            result = true;
        } catch (IOException var7) {
            throw new RuntimeException(var7);
        } finally {
            if(!result) {
                deleteDirectory(new File(destDir));
            }

        }

    }

    public static void unTarGz(String inputPath) {
        unTarGz((String)inputPath, (String)null);
    }

    public static void unTarGz(String inputPath, String destDir) {
        boolean result = false;

        try {
            File e = new File(inputPath);
            if(null == destDir) {
                destDir = e.getParentFile().getAbsolutePath();
            }

            unTarGz(e, destDir);
            result = true;
        } catch (IOException var7) {
            throw new RuntimeException(var7);
        } finally {
            if(!result) {
                deleteDirectory(new File(destDir));
            }

        }

    }

    public static void main(String[] args) throws Exception {
        unTarGz("/Users/linjianke/Downloads/2.1386849539815.1386849539815.tgz", "/Users/linjianke/Downloads/2.1386849539815.1386849539815");
    }

    public static void unTar(File file, String outputDir) throws IOException {
        TarInputStream tarIn = null;

        try {
            tarIn = new TarInputStream(new FileInputStream(file), 2048);
            createDirectory(outputDir, (String)null);
            TarEntry ex = null;

            while((ex = tarIn.getNextEntry()) != null) {
                if(ex.isDirectory()) {
                    createDirectory(outputDir, ex.getName());
                } else {
                    File tmpFile = new File(outputDir + "/" + ex.getName());
                    createDirectory(tmpFile.getParent() + "/", (String)null);
                    FileOutputStream out = null;

                    try {
                        out = new FileOutputStream(tmpFile);
                        boolean ex1 = false;
                        byte[] b = new byte[2048];

                        int ex2;
                        while((ex2 = tarIn.read(b)) != -1) {
                            out.write(b, 0, ex2);
                        }
                    } catch (IOException var23) {
                        throw var23;
                    } finally {
                        if(out != null) {
                            out.close();
                        }

                    }
                }
            }
        } catch (IOException var25) {
            throw new IOException("unTar Exception", var25);
        } finally {
            try {
                if(tarIn != null) {
                    tarIn.close();
                }
            } catch (IOException var22) {
                throw new IOException("close tarFile exception", var22);
            }

        }

    }

    public static void unTarGz(File file, String outputDir) throws IOException {
        TarInputStream tarIn = null;

        try {
            tarIn = new TarInputStream(new GZIPInputStream(new BufferedInputStream(new FileInputStream(file))), 2048);
            createDirectory(outputDir, (String)null);
            TarEntry ex = null;

            while((ex = tarIn.getNextEntry()) != null) {
                if(ex.isDirectory()) {
                    createDirectory(outputDir, ex.getName());
                } else {
                    File tmpFile = new File(outputDir + "/" + ex.getName());
                    createDirectory(tmpFile.getParent() + "/", (String)null);
                    FileOutputStream out = null;

                    try {
                        out = new FileOutputStream(tmpFile);
                        boolean ex1 = false;
                        byte[] b = new byte[2048];

                        int ex2;
                        while((ex2 = tarIn.read(b)) != -1) {
                            out.write(b, 0, ex2);
                        }
                    } catch (IOException var23) {
                        throw var23;
                    } finally {
                        if(out != null) {
                            out.close();
                        }

                    }
                }
            }
        } catch (IOException var25) {
            throw new IOException("unTarGx exception", var25);
        } finally {
            try {
                if(tarIn != null) {
                    tarIn.close();
                }
            } catch (IOException var22) {
                throw new IOException("close tarFile exception", var22);
            }

        }

    }

    public static void createDirectory(String outputDir, String subDir) {
        File file = new File(outputDir);
        if(subDir != null && !subDir.trim().equals("")) {
            file = new File(outputDir + "/" + subDir);
        }

        if(!file.exists()) {
            file.mkdirs();
        }

    }

    public static void deleteDirectory(File file) {
        if(file.isFile()) {
            file.delete();
        } else {
            File[] list = file.listFiles();
            if(list != null) {
                File[] arr$ = list;
                int len$ = list.length;

                for(int i$ = 0; i$ < len$; ++i$) {
                    File f = arr$[i$];
                    deleteDirectory(f);
                }

                file.delete();
            }
        }

    }
}
