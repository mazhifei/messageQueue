package cn.lsoft.undoner.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by vobile on 15-12-3.
 */
public class DataTimeUtils {

    public static String getCurrentTime(){
        Date currentTime = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return format.format(currentTime);
    }
}
