package cn.lsoft.undoner.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * SysConfig
 *
 * @author <A HREF="mailto:undoner@gmail.com">undoner</A>
 */

public class SysConfig {
    private static final Logger log = LoggerFactory.getLogger(SysConfig.class);

    public static long LISTENER_SLEEP_INTERVAL_FOR_MAIN_CAPTURE = 2 * 60 * 1000;
    public static long LISTENER_SLEEP_INTERVAL_FOR_BACKUP_CAPTURE = 3 * 60 * 1000;
    public static long RETRY_SLEEP_INTERVAL = 60 * 1000;
    public static int RETRY_TIME = 2;

    public static long DEVIATION_DUPLICATE_TRACK_REQUEST = 100 * 1000;// millisecond
    public static long DEVIATION_CORRECT_TRACK_REQUEST = 100 * 1000;// millisecond

    public static String MQ_EXCHANGE = "tv_ads_exchange";
    public static String MQ_PENDING_QUEUE = "tv_ads_pending_queue";
    public static String MQ_QUERY_QUEUE = "tv_ads_query_queue";
    public static String MQ_ELEMENT_QUEUE = "tv_ads_element_queue";
    public static String MQ_JOBLOG_QUEUE = "tv_ads_job_log_queue";

    static Properties pp = null;

    static {
        load();
    }

    public static void load() {
        log.info("Start Loading System Config File.");

        pp = new Properties();
        try {
            InputStream is = SysConfig.class.getClassLoader()
                    .getResourceAsStream("install.properties");
            pp.load(is);
            is.close();

            DEVIATION_DUPLICATE_TRACK_REQUEST = getLongProperty(
                    "deviation.duplicate.track.request",
                    DEVIATION_DUPLICATE_TRACK_REQUEST);
            DEVIATION_CORRECT_TRACK_REQUEST = getLongProperty(
                    "deviation.correct.track.request",
                    DEVIATION_CORRECT_TRACK_REQUEST);

            MQ_EXCHANGE = getStringProperty("mq.exchange", MQ_EXCHANGE);
            MQ_PENDING_QUEUE = getStringProperty("mq.pending.queue",
                    MQ_PENDING_QUEUE);
            MQ_QUERY_QUEUE = getStringProperty("mq.query.queue", MQ_QUERY_QUEUE);
            MQ_ELEMENT_QUEUE = getStringProperty("mq.element.queue", MQ_ELEMENT_QUEUE);
            MQ_JOBLOG_QUEUE = getStringProperty("mq.job.log.queue", MQ_JOBLOG_QUEUE);

            LISTENER_SLEEP_INTERVAL_FOR_MAIN_CAPTURE = getLongProperty(
                    "listener.sleep.interval.for.main.capture",
                    LISTENER_SLEEP_INTERVAL_FOR_MAIN_CAPTURE);
            LISTENER_SLEEP_INTERVAL_FOR_BACKUP_CAPTURE = getLongProperty(
                    "listener.sleep.interval.for.backup.capture",
                    LISTENER_SLEEP_INTERVAL_FOR_BACKUP_CAPTURE);
            RETRY_SLEEP_INTERVAL = getLongProperty("retry.sleep.interval", RETRY_SLEEP_INTERVAL);
            RETRY_TIME = getIntegerProperty("retry.time", RETRY_TIME);

        } catch (IOException e) {
            log.error(e.getMessage());
        } catch (Exception e) {
            log.error("Load SysConfig Error: ", e);
        }

        log.info("System Config File Loaded.");

    }


    private static int getIntegerProperty(String key, int defaultValue) {
        if (pp == null || pp.getProperty(key) == null) {
            return defaultValue;
        }
        try {
            return Integer.parseInt(pp.getProperty(key));
        } catch (Exception e) {
            log.error(e.toString(), e);
        }
        return defaultValue;
    }

    private static long getLongProperty(String key, long defaultValue) {
        if (pp == null || pp.getProperty(key) == null) {
            return defaultValue;
        }
        try {
            return Long.parseLong(pp.getProperty(key));
        } catch (Exception e) {
            log.error(e.toString(), e);
        }
        return defaultValue;
    }

    private static String getStringProperty(String key, String defaultValue) {
        if (pp == null || pp.getProperty(key) == null) {
            return defaultValue;
        }
        return pp.getProperty(key).trim();
    }

    public static void main(String args[]) {
        load();
    }

}
