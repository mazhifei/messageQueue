package cn.lsoft.undoner.service;

import cn.lsoft.undoner.model.RabbitQueue;

import java.util.List;

/**
 * @author <A HREF="mailto:undoner@gmail.com">undoner</A>
 * @date 03-11-2014
 */
public interface RabbitMQService {
    int reSend(String fromQueue, String toQueue,Integer limit);

    List<RabbitQueue>  listMessageReadyAllQueue();

    List<RabbitQueue>  listMessageReadyDeadQueue();

    List<RabbitQueue>  listMessageReadyQueue();
}
