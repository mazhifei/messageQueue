package cn.lsoft.undoner.service.impl;

import cn.lsoft.undoner.dao.MessageDao;
import cn.lsoft.undoner.dao.UserDao;
import cn.lsoft.undoner.model.User;
import cn.lsoft.undoner.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by vobile on 15-12-8.
 */
@Service
public class UserServiceImpl implements UserService {

    @Qualifier("userDaoImpl")
    @Autowired
    private UserDao userDao;

    @Override
    public boolean saveUser(User user) {
        return false;
    }

    @Override
    public List<User> queryUserByUsername(String userName) {
        return null;
    }

    @Override
    public boolean queryUserByUser(User user) {
        List<User> usersList = userDao.queryUserByUser(user);
        if(usersList.size()>0){
            return true;
        }else{
            return false;
        }

    }

}
