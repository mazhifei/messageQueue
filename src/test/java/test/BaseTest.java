package test;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author <A HREF="mailto:undoner@gmail.com">undoner</A>
 * @date 05-06-2014
 */
@ContextConfiguration(locations = "classpath:spring/springMVC.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public abstract class BaseTest {
}
